Omar this is the perfect task to get you started.

I�ve attached a list of words.
What I need is to have two other lists created from that one list
List1:  The definitions of the word, The part of speech that goes with each definition in 1 line eg. Word##Noun||defintion1||definition2||definition3##Verb||defintion1||definition2
List2: The synonyms for each word with comma delimiters eg. Word,Syn1,Syn2,Syn3

I would suggest using an IDE that offers debugging to python like VS Community 2013 and PY TOOLs or even eclipse with python plugin.
You will need to more than likely query a dictionary website and scrape the HTML. I suggest beautifulsoup4 module if you go that route, along with requests module.


Python interactive window. Type $help for a list of commands.
>>> import sys
>>> sys.version
'2.7.9 (default, Dec 10 2014, 12:24:55) [MSC v.1500 32 bit (Intel)]'