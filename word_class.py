from operator import itemgetter

class Word:
    #don't rearrange
    parts_of_speech_dictionary = dict({"abbr" : "Abbreviation", "adj":"Adjective", "interj" : "Interjection", "n":"Noun",  "v":"Verb" })
    parts_of_speech_abbrv = ["abbr", "adj", "interj", "n", "v"]
    parts_of_speech = ["Adjective", "Interjection", "Noun", "Verb"]
    empty_definition = " "

    def __init__(self, name):
        self.name = name

    @property
    def name(self) :
        return self.name

    @name.setter
    def name(self, value) :
        if value :
            self.name = value

    @property
    def definitions(self) :
        return self.definitions
    
    @definitions.setter
    def definitions(self, value) :
        self.definitions = value            

    @property
    def synonyms(self) :
        return self.synonyms
    
    @synonyms.setter
    def synonyms(self, value) :
        self.synonyms = value

    def generate_definitions(self, limit) :
        try :
            #print('\n Generating Definitions...')

            output = self.name
            if limit is None : return
            if self.definitions is None : 
                print('\n NO DEFINITION FOR WORD: ' + self.name)
                for i in self.parts_of_speech :
                    for x in range(0, limit) : 
                        if x == 0 :
                            output +="##" + i + "||" + self.empty_definition
                        else :
                            output+= "||" + self.empty_definition
                return output
            
            nouns = []
            adjectives = []
            verbs = []
            intj = []

            for defintion in self.definitions :
                if 'noun' in defintion['speech'] :
                    nouns.append(defintion['definition'])
                elif 'adjective' in defintion['speech'] :
                    adjectives.append(defintion['definition'])
                elif 'verb' in defintion['speech'] :
                    verbs.append(defintion['definition'])
                elif 'interjection' in defintion['speech'] :
                    intj.append(defintion['definition'])

            #Lazy.... :|
            #Should fix when I have time
            #Noun
            for x in range(0, limit) :
                if x == 0 :
                    output +="##" + "Noun"
                if x < len(nouns):
                     output += "||" + nouns[x]
                else :
                     output += "||" + self.empty_definition
            
            #Adj
            for x in range(0, limit) :
                if x == 0 :
                    output +="##" + "Adjective"
                if x < len(adjectives):
                     output += "||" + adjectives[x]
                else :
                     output += "||" + self.empty_definition

           #Verb
            for x in range(0, limit) :
                if x == 0 :
                    output +="##" + "Verb"
                if x < len(verbs):
                     output += "||" + verbs[x]
                else :
                     output += "||" + self.empty_definition

            #Interjection
            for x in range(0, limit) :
                if x == 0 :
                    output +="##" + "Interjection"
                if x < len(intj):
                     output += "||" + intj[x]
                else :
                     output += "||" + self.empty_definition

            return output.encode("utf-8")
        except Exception as e :
            print(str(e))

    def generate_synonyms(self, limit) :
        try :

            output = self.name + ","
            if limit is None : return output
            if self.synonyms is None: 
                print('\n NO SYNONYM FOR WORD: ' + self.name)
                for x in range(0, limit - 1) : output+= ","
                return output.encode("utf-8")
        
            for x in range(0, limit) :
                if x < len(self.synonyms) :
                    if x == (len(self.synonyms) -1 ) : output += self.synonyms[x] 
                    else : output += self.synonyms[x] + ","
                else :
                    output+= ","

            return output.encode("utf-8")
        except Exception as e :
            print(str(e))