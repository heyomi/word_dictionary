from operator import itemgetter

class Word:
    #don't rearrange
    parts_of_speech_dictionary = dict({"abbr" : "Abbreviation", "adj":"Adjective", "interj" : "Interjection", "n":"Noun",  "v":"Verb" })
    parts_of_speech_abbrv = ["abbr", "adj", "interj", "n", "v"]
    parts_of_speech = ["Abbreviation", "Adjective", "Interjection", "Noun", "Verb"]
    empty_definition = " "

    def __init__(self, name):
        self.name = name

    @property
    def name(self) :
        return self.name

    @name.setter
    def name(self, value) :
        if value :
            self.name = value

    @property
    def definitions(self) :
        return self.definitions
    
    @definitions.setter
    def definitions(self, value) :
        self.definitions = value            

    @property
    def synonyms(self) :
        return self.synonyms
    
    @synonyms.setter
    def synonyms(self, value) :
        self.synonyms = value

    def generate_definitions(self, limit) :
        try :
            #print('\n Generating Definitions...')

            output = self.name
            if limit is None : return
            if self.definitions is None : 
                print('\n NO DEFINITION FOR WORD: ' + self.name)
                for x in range(0, limit - 1) : output+= "||"
                return output
                return
            
            #for key, value in sorted_list.items() :
            for index, value in enumerate(self.parts_of_speech_abbrv) :
                for x in self.definitions : #definitions
                    if not value in x['speech'] : # part of speech are sorted. if value isnt present then it doesnt exist; empty
                        for i in range(0, limit) :
                            if i == 0 :
                                output +="##" + self.parts_of_speech[index] + "||" #Identify part of speech by '##'
                            else :
                                output += "||" + self.empty_definition
                        continue
                    else :
                        current_limit = 1 if not isinstance(x['definition'], list) else limit if len(x['definition']) >= limit else len(x['definition'])
                        
                        if not isinstance(x['definition'], list) :
                            for i in range(0, limit) :
                                if i == 0 :
                                        output +="##" + self.parts_of_speech[index] + "||" + x['definition'].strip() #Identify part of speech by '##'                                
                                else : 
                                    output += "||" + self.empty_definition
                        else :
                            current_limit = limit if len(x['definition']) >= limit else len(x['definition']) # the limit may be greater than the amount of definions retrieved
                            for i in range(0, limit) :
                                if i == 0 :
                                        output +="##" + self.parts_of_speech[index] + "||"
                                if current_limit >= i :
                                    if i == 0 :
                                        output +="##" + self.parts_of_speech[index] + "||" + x['definition'][i].strip() #Identify part of speech by '##'
                                    elif current_limit != i:
                                        output += "||" + x['definition'][i].strip()                                
                                else : 
                                    output += "||" + self.empty_definition


            return output
        except Exception as e :
            print(str(e))
        #finally :
            #print('\n...done')

    def generate_synonyms(self, limit) :
        try :
            #print('\n Generating Synonyms...')

            output = self.name + ","
            if limit is None : return output
            if self.synonyms is None: 
                print('\n NO SYNONYM FOR WORD: ' + self.name)
                for x in range(0, limit - 1) : output+= ","
                return output
        
            for x in range(0, limit - 1) :
                if x < len(self.synonyms) :
                    output += self.synonyms[x].strip() if x == 0 else (output + "," + self.synonyms[x].strip())
                else :
                    output+= ","

            return output
        except Exception as e :
            print(str(e))
        #finally :
            #print('\n...done')