from bs4 import BeautifulSoup

import requests, os, io, sys, codecs, word_class,traceback,linecache,re,time
import multiprocessing as mp
from timeit import default_timer as timer
from threading import Thread

from word_class import Word
from operator import itemgetter
from collections import OrderedDict

#file directories
words_data_in_dir = os.path.dirname(__file__) + '\data\words.in.csv';
words_data_in_dir_parts = os.path.dirname(__file__) + '\data\words.in';
definitions_out_dir = os.path.dirname(__file__) + '\data\definitions.out.csv';
synonyms_out_dir = os.path.dirname(__file__) + '\data\synonyms.out.csv';
sample_definition = os.path.dirname(__file__) + '\data\sample_definition.html';
test_words_data_in_dir = os.path.dirname(__file__) + '\data\words.in.test.csv';

DICTIONARY_URL = "http://www.dictionary.reference.com/browse/"
SYNONYM_URL = "http://www.synonym.com/synonyms/"
words = []
MAX_WORDS_PER_SPEECH = 3 #limit on how many definitons
MAX_SYNONYMS = 5

#Prints exceptions in detail
def print_exception():
    exc_type, exc_obj, tb = sys.exc_info()
    f = tb.tb_frame
    lineno = tb.tb_lineno
    filename = f.f_code.co_filename
    linecache.checkcache(filename)
    line = linecache.getline(filename, lineno, f.f_globals)
    print 'EXCEPTION IN ({}, LINE {} "{}"): {}'.format(filename, lineno, line.strip(), exc_obj)

#Scrapes dictionary website to get definition and synonym block for each word
def scrape(word):
    try:
        
        the_word = Word(word)
        the_word.synonyms = request_synonyms(word)
        the_word.definitions = request_definitions(word)

        return the_word
    except Exception as e:  
        print(str(e))
        print_exception()

def request_definitions(word):
    print('Requesting defintions for ' + word)
    
    try:
        request  = requests.get(DICTIONARY_URL + word)
        if request is None : return
        
        data = request.content
        if data is None : return
        
        soup = BeautifulSoup(data)
        if soup is None : return

        #the_word = Word(word)

        #capture synonyms
        definition_sections = soup.findAll("div", { "class" : "def-content" })

        if definition_sections :
            return process_definitions(word, definition_sections)    
        else : return None
    except Exception as e:  
            print(str(e))
            print_exception()

def request_synonyms(word):
    print('Requesting synonyms for ' + word)

    try :
        request  = requests.get(SYNONYM_URL + word)
        if request is None : return
        
        data = request.content
        if data is None : return
        
        soup = BeautifulSoup(data)
        if soup is None : return

        #the_word = Word(word)

        #capture synonyms
        synonym_section = soup.findAll("span", { "class" : "equals" })
        synonym_section2 = soup.findAll("div", { "class" : "Accent Sense" })

        if synonym_section2 : synonym_section += synonym_section2

        if synonym_section :
            return process_synonyms(word, synonym_section)    
        else : return None

    except Exception as e:  
        print(str(e))
        print_exception()


def output_definitions(words, is_new_file):
    try :
        print('\nWriting definitions...')
        print('--------------------')
        file = open(definitions_out_dir, 'a') if not is_new_file else open(definitions_out_dir, 'w')
        headers = "Word"
        tmp = []
        if is_new_file :
            for value in Word.parts_of_speech :
                headers += "##" + value
                
                #avoid duplicate parts of speech in heading/data; v & vb - Verb
                if value in tmp : continue
                else : tmp.append(value)

                for x in range(MAX_WORDS_PER_SPEECH) :
                    headers += "||definition" + str(x+1)
            file.write(headers + "\n")

        for word in words :
            #if word.definitions is not None:
            file.write(word.generate_definitions(MAX_WORDS_PER_SPEECH) + "\n")
            print(word.name + ' ...done')

    except Exception as e :
        print(str(e))
        print_exception()
    finally :
        file.close()
        

def output_synonyms(words, is_new_file) :
    try :
        print('\nWriting synonyms...')
        print('--------------------')
        file = open(synonyms_out_dir, 'a') if not is_new_file else open(synonyms_out_dir, 'w')
        headers = "Word,"
        if is_new_file :
            for x in range(MAX_SYNONYMS) :
                headers += "Synonym" + str(x+1) + "," if x < (MAX_SYNONYMS - 1) else "Synonym"
            file.write(headers + "\n")

        for word in words :
            file.write(word.generate_synonyms(MAX_SYNONYMS) + "\n")
            print(word.name + ' ...done')
            #print('\n Done!')

    except Exception as e :
        print(str(e))
        print_exception()
    finally :
        file.close()


#Gets the difnitions of a given word with respect to the part of speech.
def process_definitions(the_word, dictionary_section) :
    try :
        definitions = []

        for item in dictionary_section:
            speech = item.parent.parent.contents[1].text.strip()
            definition = item.contents[0].strip();
            
            if 'verb ' in speech : speech = 'verb'
            elif 'noun' in speech : speech = 'noun'
            elif 'adjective' in speech : speech = 'adjective'
            elif 'interjection' in speech : speech = 'interjection'

            if speech is None : return 

            if speech.capitalize() not in Word.parts_of_speech : continue

            if len(definition) < 3 : 
                if item.contents[1] and item.contents[1].string is not None :
                    if len(item.contents[1].string.strip()) > 3 :
                        definition = item.contents[1].string.strip();
                elif item.contents[1].contents is not None :
                    definition = item.contents[1].contents[1].string
                elif item.contents[2] and item.contents[2].string is not None :
                    if len(item.contents[2].string.strip()) > 3 :
                        definition = item.contents[2].string.strip();
                else : continue

            definitions.append({'speech' : speech, 'definition' : definition})

        if definitions :
            return sorted(definitions, key=itemgetter('speech'))
        else : return None

    except Exception as e :
        print(str(e) + ' while getting definitions for WORD: ' + the_word)
        print_exception()


#Gets synonyms for each word.    
def process_synonyms(the_word, synonym_section) :
    try :
        print('Getting synonyms... ' + the_word)
        synonyms = list()
        synonymsStr = ''
        count = 0
        for item in synonym_section:
            if len(item) < 1 : continue
            #check for <div class="Accent Sense">Sense 1:</div> sibling for more synonyms
            if 'Sense ' in item.text : synonymsStr += item.next_sibling.strip() 
            else : synonymsStr += item.text.strip() + ','
                       
        if synonymsStr :
            synonyms = synonymsStr.replace(the_word,'').replace(', ,',',').replace(',,',',').split(',') #remove the_word from synonym list
            synonyms = [item.strip() for item in synonyms if item.strip() != ""]  #trim all words
            synonyms = list(set(synonyms))  #removes duplicate words

            return synonyms[:MAX_SYNONYMS]
        else : return None

    except Exception as e :
        print(str(e) + ' while getting synonyms for WORD: ' + the_word.name)
        print_exception()

#Reads in words and processes them
def extractor(file_dir, is_new_file):
    try:
        start = timer()
       
        #words_data_in_dir
        print('Starting...\nGetting words...\nCleaning words..\n')
        file = codecs.open(file_dir, "r", "utf-8-sig")
        
        for line in file:
            if line is None : continue
            elif len(line.strip()) < 2 : continue
            else :
                words.append(scrape(line.strip()))
    
        print('Exporting words...')
        #export words
        output_synonyms(words, is_new_file)
        output_definitions(words, is_new_file)
    except Exception as e:  
        print(str(e))
        print_exception()
    finally:
        file.close()
        print("\n DONE.")
        print timer() - start
       
#RUN
def Main():
    try:
        # Define an output queue
        output = mp.Queue()

        threads = []
        limit = 9

        for x in range(0,limit) :
            threads.append(Thread(target=extractor, args=(words_data_in_dir_parts + "-00" + str(x) + ".csv",True)))
            threads[x].start()

    except Exception as e:  
        print_exception()

Main()
