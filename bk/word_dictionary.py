from bs4 import BeautifulSoup

import requests, os, io, sys, codecs, word_class,traceback,linecache,re
from word_class import Word
from operator import itemgetter

#file directories
words_data_in_dir = os.path.dirname(__file__) + '\data\words.in.sample.csv';
definitions_out_dir = os.path.dirname(__file__) + '\data\definitions.out.csv';
synonyms_out_dir = os.path.dirname(__file__) + '\data\synonyms.out.csv';
sample_definition = os.path.dirname(__file__) + '\data\sample_definition.html';
test_words_data_in_dir = os.path.dirname(__file__) + '\data\words.in.test.csv';

DICTIONARY_URL = "http://www.thefreedictionary.com/" #consider using another site than loads faster

ABBREVIATION = "abbr"
words = []
MAX_WORDS_PER_SPEECH = 3 #limit on how many definitons
MAX_SYNONYMS = 5

def print_exception():
    exc_type, exc_obj, tb = sys.exc_info()
    f = tb.tb_frame
    lineno = tb.tb_lineno
    filename = f.f_code.co_filename
    linecache.checkcache(filename)
    line = linecache.getline(filename, lineno, f.f_globals)
    print 'EXCEPTION IN ({}, LINE {} "{}"): {}'.format(filename, lineno, line.strip(), exc_obj)

#Scrapes dictionary website to get definition and synonym block for each word
def scrape(word):
    try:
        print('Scraping website..')

        request  = requests.get(DICTIONARY_URL + word)
        if request is None : return
        
        data = request.content
        if data is None : return
        
        soup = BeautifulSoup(data)
        if soup is None : return

        #capture defintions
        dictionary_section = soup.findAll("div", { "class" : "pseg" })

        #capture synonyms
        synonym_section = soup.findAll("div", { "class" : "Rel" })

        the_word = Word(word)
        if dictionary_section :
           the_word.definitions = process_definitions(the_word, dictionary_section, False)  
        # http://www.thefreedictionary.com/aalii definition isn't being captured; diff html
        elif soup.findAll("div", { "id" : "Definition" }) :
            the_word.definitions = process_definitions(the_word, soup.findAll("div", { "id" : "Definition" }),True)
        else : the_word.definitions = None

        if synonym_section :
            the_word.synonyms = process_synonyms(word, synonym_section)    
        else : the_word.synonyms = None

        return the_word
    except Exception as e:  
        print(str(e))
        print_exception()


def output_definitions(words, is_new_file):
    try :
        print('\nWriting definitions...')
        print('--------------------')
        file = open(definitions_out_dir, 'a') if not is_new_file else open(definitions_out_dir, 'w')
        headers = "Word"
        tmp = []
        if is_new_file :
            for value in Word.parts_of_speech :
                headers += "##" + value
                
                #avoid duplicate parts of speech in heading/data; v & vb - Verb
                if value in tmp : continue
                else : tmp.append(value)

                for x in range(MAX_WORDS_PER_SPEECH) :
                    headers += "||definition" + str(x+1)
            file.write(headers + "\n")

        for word in words :
            if word.definitions is not None:
                file.write(word.generate_definitions(MAX_WORDS_PER_SPEECH) + "\n")
                print(word.name + ' ...done')
                #print('\n Done!')

    except Exception as e :
        print(str(e))
        print_exception()
    finally :
        file.close()
        

def output_synonyms(words, is_new_file) :
    try :
        print('\nWriting synonyms...')
        print('--------------------')
        file = open(synonyms_out_dir, 'a') if not is_new_file else open(synonyms_out_dir, 'w')
        headers = "Word,"
        if is_new_file :
            for x in range(MAX_SYNONYMS) :
                headers += "Synonym" + str(x+1) + "," if x < (MAX_SYNONYMS - 1) else "Synonym"
            file.write(headers + "\n")

        for word in words :
            file.write(word.generate_synonyms(MAX_SYNONYMS) + "\n")
            print(word.name + ' ...done')
            #print('\n Done!')

    except Exception as e :
        print(str(e))
        print_exception()
    finally :
        file.close()


#Gets the difnitions of a given word with respect to the part of speech.
def process_definitions(the_word, dictionary_section, bool) :
    try :
        print('Getting Definitions... ' +the_word.name)
        #file = open(definitions_out_dir, 'w')
        definitions = []

        for item in dictionary_section:
            speech = ''
            definition = []
            
            if bool :
                if item.findAll('i') :
                    speech = item.findAll('i')[1].contents[0].replace(".","").strip()
                    if len(speech) == 0 : continue

                    #replace duplicated abbreviations (based on dictionary site)
                    if speech == "vb" or speech == "vintr" or speech == "vi" or speech == "trv" : speech = "v"
                    if speech not in Word.parts_of_speech_abbrv : continue
                    definitions.append({'speech' : speech, 'definition' : item.contents[0].contents[len(item.contents[0].contents) - 2].text})
                elif item.findAll('b') :
                    if item.findAll('b')[0].contents[0]:
                        if item.findAll('b')[0].contents[0] == "a." :
                            speech = "adj"
                        elif item.findAll('b')[0].contents[0] == "n." :
                            speech = "n"
                        elif item.findAll('b')[0].contents[0] == "v." :
                            speech = "v"

                        if not speech : definitions.append({'speech' : speech, 'definition' : item.contents[0].contents[len(item.contents[0].contents) - 1].text})
                continue

            #grab part of speech before going any further.
            if not item.contents[0] : break
            #elif ABBREVIATION in item.contents[0].text : continue #skip abbri. part of speech
            else: speech = item.contents[0].text.replace(".","")
            if len(speech) == 0 : continue

            #replace duplicated abbreviations (based on dictionary site)
            if speech == "vb" or speech == "vintr" or speech == "vi" or speech == "trv" : speech = "v"
            if speech not in Word.parts_of_speech_abbrv : continue

            definitionContent = item.findAll("div", { "class" : "sds-list" })

            if speech == ABBREVIATION :
                definitionContent = item.contents

            if not definitionContent :
                definitionContent = item.contents

            if definitionContent : #list of definitions
                count = len(definitionContent) if len(definitionContent) < MAX_WORDS_PER_SPEECH else MAX_WORDS_PER_SPEECH
                currentDefinitions = []

                for x in range(count):
                    if item.findAll("div", { "class" : "sds-list" }) :
                        if x < len(item.findAll("div", { "class" : "sds-list" })) :
                            currentDefinitions.append(item.findAll("div", { "class" : "sds-list" })[x].text)
                    if (item.findAll("div", { "class" : "ds-single" })) : #special cases where item.contents was used and only ds-single was there
                        currentDefinitions.append(item.findAll("div", { "class" : "ds-single" })[0].text)
                        continue
                    if not item.findAll("div", { "class" : "ds-list" }) :
                        continue
                    else :
                        if x < len(item.findAll("div", { "class" : "ds-list" })) :
                            currentDefinitions.append(item.findAll("div", { "class" : "ds-list" })[x].text)

                definition = currentDefinitions
                
            #single definition
            elif (item.findAll("div", { "class" : "ds-single" })) : 
                definition.append(item.findAll("div", { "class" : "ds-single" })[0].text)
            else : #text is not apart of definition list
                break
            
            #maintains defintion list
            definitions.append({'speech' : speech, 'definition' : definition})

        if definitions :
            return sorted(definitions, key=itemgetter('speech'))
        else : return None

    except Exception as e :
        print(str(e) + ' WORD: ' + the_word.name)
        print_exception()
    #finally :
        #file.close()


#Gets synonyms for each word.    
def process_synonyms(the_word, synonym_section) :
    try :
        print('Getting synonyms... ' + the_word)
        synonyms = []
        limit = len(synonym_section) if len(synonym_section) < MAX_SYNONYMS else MAX_SYNONYMS
        count = 0
        for item in synonym_section:
            if count == limit or not item.contents[0] :
                continue
            else :
                if item.contents[0] : synonyms.append(re.sub("See Synonyms at\s+\w+\.", "",item.contents[0].text))
                else :
                    for x in range(4): #dig a little deeper in xpath for more synonyms
                        if len(item.contents) < 4 or item.contents[x] is None or item.contents[x].name is None : continue
                        else : synonyms.append(re.sub("See Synonyms at\s+\w+\.", "",item.contents[x].text))
            count=+ 1
        if synonyms :
            return synonyms
        else : return None

    except Exception as e :
        print(str(e) + ' WORD: ' + the_word.name)
        print_exception()

#Reads in words and processes them
def main():
    try:
        print('Starting...\nGetting words...\nCleaning words..\n')
        file = codecs.open(test_words_data_in_dir, "r", "utf-8-sig")
        for line in file:
            if line is None : break
            else :
                words.append(scrape(line.strip()))
    
        print('Exporting words...')
        #export words
        output_synonyms(words, True)
        output_definitions(words, True)
    except Exception as e:  
        print(str(e))
        print_exception()
    finally:
        file.close()
       
#RUN
main()